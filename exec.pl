#!/usr/bin/perl
use strict;
use warnings;

my $file = 'public/result.html';
my $command = join(' ', @ARGV);
chomp $command;

# print $command."\n";

sub cmd_to_file
{
    clear_file();
    open(OUT, ">>$file")    or die "Failed: $!";
    open(CMD, "$command |") or die "Failed: $!";
    while(my $str = <CMD>)
    {
        `echo "$str\<br \/\>" >> $file`;
    }
    close CMD;
    close OUT;

    return 1;
}

sub clear_file
{
    `echo '' > $file`;
    return 1;
}

cmd_to_file();