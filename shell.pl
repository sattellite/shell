#!/usr/bin/perl
use strict;
use warnings;

use utf8;
use Mojolicious::Lite;

app->renderer->encoding('utf-8');

get '/' => sub{
    my $self = shift;

    $self->render(template => 'index');
};

post '/exec' => sub {
    my $self = shift;
    my $cmd = $self->req->param('cmd');
    chomp $cmd;

    system("./exec.pl $cmd &");

    $self->render(template=>'index');
    $self->redirect_to('/');

};

app->start;

__DATA__

@@ index.html.ep
% layout 'default', title => 'shell';

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        <meta http-equiv="pragma" content="no-cache">
        <link rel="stylesheet" href="main.css">
        <title><%= $title %></title>
    </head>
    <body>
        <script>
        function getXmlHttp()
        {
            var isMSIE = /*@cc_on!@*/false;

            var xmlhttp;
            if (isMSIE) {   try {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                    try {
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (E) {
                            xmlhttp = false;
                    }
            }
            }
            if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
                    xmlhttp = new XMLHttpRequest();
            }
            return xmlhttp;
                        }

        function updateFunc()
        {
            var xmlhttp = getXmlHttp();
            xmlhttp.open('GET', 'result.html', false); // false - синхронно, true - асинхронно.
            xmlhttp.send(null);

            document.getElementById('result').innerHTML = xmlhttp.responseText; 
        }

        window.onload = updateFunc;
        setInterval(updateFunc,500);

        </script>

        <div class="content">
            <div class="name header">Запуск shell команд в realtime*.</div>
            <form action='/exec' method='post'>
                <table>
                    <tr>
                        <td class='cmd'>
                            Команда:
                        </td>
                        <td class='form'>
                            <input class='enter' type='text' name='cmd' size='12'>
                        </td>
                        <td class='cmd'>
                            <input class='button' type='submit' name='exec' value='Выполнить'>
                        </td>
                    </tr>
                </table>
            </form>
            <div class="serv">
                <div id="result"></div>
            </div>
            <div class="footer">* Данные обновляются каждых 0.5 секунды.</div>
        </div>
    </body>
</html>

@@ main.css
@import url(http://fonts.googleapis.com/css?family=Philosopher:700,700italic|Tenor+Sans&subset=latin,cyrillic,cyrillic-ext);

body {
    padding: 0px;
    margin: 0px;
    background: #EEFFDF;
    font-family: 'Tenor Sans', sans-serif;
    color: #222222;
}

table {
    width: 100%;
    margin-left: 30px;
    padding-right: 30px;    
}

.cmd {
    width: 30px;
}

.form {
    width: 662px;
}

.button {
    position: relative;
    height: 28px;
    font-family: 'Tenor Sans', sans-serif;
    font-size: 16px;
}

.content {
    display: block;
    width: 732px;
    margin: 0 auto;
}

.enter {
    height: 22px;
    width: 100%;
    font-size: 22px;
}

.serv {
    display: inline-block;
    padding: 20px;
    width: 100%;
    margin-top: 20px;
    border-radius: 3px;
    background: #f9f9f9;
    box-shadow: 1px 1px 3px #183509;
    top: 120px;
}

.name {
    font-weight: bold;
    color: #097500;
    font-family: 'Philosopher', cursive;
    font-size: 22px;
    text-align: center;
    text-decoration: none;
    text-shadow: 0 1px 0 #5FA15A,0 0 6px #A2D29E ,0 0 30px #D0F0C0;
}   

.header {
    padding: 30px 0 10px 0;
    font-size: 26px;
}

.footer {
    position: absolute;
    padding-bottom: 5px;
    bottom: 0;
    font-size: 12px;
    z-index: -10;
}

# For get html/txt file http://stackoverflow.com/questions/1981815/jquery-read-a-text-file
